# vfs-rebuild

A portmod module for marking packages as needing to be rebuilt when files in the OpenMW VFS change.
