from setuptools import setup

setup(
    name="vfs-rebuild",
    version="0.1.0",
    description="Portmod module for rebuilding packages when the vfs changes",
    author="Benjamin Winger",
    author_email="bmw@disroot.org",
    url="https://gitlab.com/portmod/vfs-rebuild",
    modules=["vfs_rebuild"],
    data_files=[("modules", ["vfs-rebuild.pmodule"])],
)
