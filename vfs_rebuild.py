# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import fnmatch
import os
import re
from functools import lru_cache
from typing import Dict, Generator, List, Optional, Pattern, Union

from configtool.openmw import find_file, list_dir
from portmod.parsers.manifest import FileType, Manifest, ManifestEntry


def _fnmatch_list_to_re(patterns: List[str]) -> Pattern:
    def to_re(value: Union[str, Pattern]):
        """
        Converts fn-match string into a regular expression string

        Note that normpath may not work as expected with fn-match strings
        if forward-slashes are present inside bracketed ranges (e.g. [/../]).
        """
        if isinstance(value, Pattern):
            return value
        return fnmatch.translate(os.path.normpath(value))

    flags = 0
    if os.environ.get("CASE_INSENSITIVE_FILES", False):
        flags = re.IGNORECASE
    return re.compile("|".join(map(to_re, patterns)), flags=flags)


def _rebuild_path(package_name: str, root: Optional[str] = None):
    return os.path.join(
        root or os.environ["ROOT"], "lib", "rebuild_files", package_name
    )


@lru_cache()
def installed_rebuild_files(package_name: str) -> Optional[Manifest]:
    rebuild_path = _rebuild_path(package_name)
    if os.path.exists(rebuild_path):
        return Manifest(rebuild_path)
    return None


def create_rebuild_manifest(
    package_name: str, rebuild_files: List[str], root: Optional[str] = None
):
    """
    Creates a rebuild manifest for the given package

    args:
        package_name: The package name.
            This will be used to determine the path of the file for storage
        rebuild_files: VFS paths to monitor and use to trigger rebuilds
    """
    manifest = Manifest()
    for entry in get_rebuild_manifest(rebuild_files):
        manifest.add_entry(entry)
    os.makedirs(os.path.dirname(_rebuild_path(package_name, root=root)), exist_ok=True)
    manifest.write(_rebuild_path(package_name, root=root))


def get_rebuild_manifest(patterns: List[str]) -> Generator[ManifestEntry, None, None]:
    """
    Yields manifest entries with hashes for files which this package tracks.

    If any such file changes compared to when the package was installed,
    the package should be rebuilt.
    """
    if not patterns:
        return None

    def handle_pattern(pattern: str, base_path: str = ""):
        if re.search(r"[*?]|\[.*\]", pattern):
            # Check pattern one component at a time
            if os.path.dirname(pattern):
                directory, _, child = pattern.partition("/")
                if re.search(r"[*?]|\[.*\]", directory):
                    # Find all directories matching pattern
                    for path in list_dir(base_path):
                        if fnmatch.fnmatch(path, directory):
                            if base_path:
                                path = base_path + "/" + path
                            yield from handle_pattern(child, path)
                else:
                    if base_path:
                        directory = base_path + "/" + directory
                    yield from handle_pattern(child, directory)
            else:
                # Match pattern against all files in base_path
                # We delay this so that it can be combined with similar patterns
                if base_path.lower() in dir_patterns:
                    dir_patterns[base_path.lower()].append(pattern)
                else:
                    dir_patterns[base_path.lower()] = [pattern]
        else:  # Explicit file path without wildcards
            if base_path:
                path = base_path + "/" + pattern
            else:
                path = pattern
            yield ManifestEntry.from_path(FileType.MISC, find_file(path), path)

    dir_patterns: Dict[str, List[str]] = {}
    # FIXME: Should respect CASE_INSENSITIVE_FILES
    # Currently assumes it's True
    for pattern in patterns:
        # To be efficient, filter patterns by type and directory
        # Avoiding traversing parts of the tree which the patterns
        # cannot touch
        yield from handle_pattern(pattern)

    def normjoin(directory, file):
        if directory:
            return os.path.normpath(directory + "/" + file)
        else:
            return file

    for directory, patterns in dir_patterns.items():
        regex = _fnmatch_list_to_re(patterns)

        for file in list_dir(directory):
            if regex.match(file):
                yield ManifestEntry.from_path(
                    FileType.MISC,
                    find_file(normjoin(directory, file)),
                    normjoin(directory, file),
                )
